
# openpyxl module required for parsing execel sheet 
import openpyxl
import os

# Give the location of the file  
stock_file_name = "2016-11_SP_10years.xlsx"

if ( not os.path.isfile(stock_file_name)):
    print("File not found, keep the file 2016-11_SP_10years.xlsx in the current directoy or enter the complete path with the filename")
    exit()

stockcalc_obj = openpyxl.load_workbook(stock_file_name) 
  
# stocksheet object is created 
stockcalc_sheet_obj = stockcalc_obj.active 

# count of total number of rows for iteration
max_row = stockcalc_sheet_obj.max_row

#intialization of temporary variables 
temp_highest,temp_lowest = 0,0

#looping through all the rows(dates) in the sheet, ignoring the first row as it has the headers
for i in range(2, max_row + 1):
    try:
        open_of_the_day = stockcalc_sheet_obj.cell(i, 3).value    #open of the day value taken from the 3rd column 
        high_of_the_day = stockcalc_sheet_obj.cell(i, 4).value    #high of the day value taken from the 4th column 
        low_of_the_day = stockcalc_sheet_obj.cell(i, 5).value     #low of the day value taken from the 5th columnt

        #Calculating the biggest positive change date
        var_high_chng = (high_of_the_day - open_of_the_day)
        if ( var_high_chng > temp_highest ):
            temp_highest = var_high_chng
            high_chng_date = stockcalc_sheet_obj.cell(i, 1).value

    
        #Calculating the biggest negative change date
        var_low_chng = (open_of_the_day - low_of_the_day )
        if ( var_low_chng > temp_lowest ):
            temp_lowest = var_low_chng
            low_chng_date = stockcalc_sheet_obj.cell(i, 1).value
    
    #Breaking from the loop after getting exception(row has Highest tab word in the excel sheet )
    except TypeError:
        #Stopping processing once recieve TyepError(instead of date we get different data in the sheet)
        break

print ("Biggest positive change date is -" , high_chng_date)
print ("Biggest negative change date is -" , low_chng_date)

        