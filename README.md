# Stock Biggest Change Date Calculator

Calculating biggest positive and negative change dates for a stock 


### Prerequisites


```
python3 (install python3)
pip install openpyxl (install openxyl module using pip)

```

### Executing the script


```
git clone https://bitbucket.org/revanth_parvathaiah/stockchangecalculator.git
cd stockchangecalculator/
python biggest_change_date_calc.py

```

## Authors

* *Revanth Parvathaiah* - (revanthec@gmail.com)

